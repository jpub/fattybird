﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FattyBird
{
    class ManagerBarrel
    {
        private static ManagerBarrel msInstance = null;
        Random r = null;
        private int totalBarrelsCreated = 0;
        private int totalBarrelsCollided = 0;
        private int totalBarrelsNotCollided = 0;

        private ManagerBarrel()
        {
            r = new Random( (int)DateTime.Now.Ticks );
            ModelScore model = new ModelScore();            
            model.Collided = 0;
            model.NotCollided = 0;
            DbScore.getInstance().add( model );
            /*
            ModelBarrel model2 = new ModelBarrel();
            model2.GapOpeningHeight = 50;
            model2.GapCenterX = 400;
            model2.GapCenterY = 100;
            model2.BarrelWidth = 20;
            DbBarrel.getInstance().update( "2", model2 );
             * */
        }

        static public ManagerBarrel getInstance()
        {
            if ( msInstance == null )
            {
                msInstance = new ManagerBarrel();
                return msInstance;
            }
            return msInstance;
        }

        public void getScore( out int collided, out int notCollided )
        {
            collided = this.totalBarrelsCollided;
            notCollided = this.totalBarrelsNotCollided;
        }

        public void updateBarrels()
        {
            int totalBarrelInDb = DbBarrel.getInstance().count();
            if ( totalBarrelInDb == 0 )
            {
                this.createBarrel();                
                return;
            }

            string lastBarrelId = this.totalBarrelsCreated.ToString();
            string firstBarrelId = ( this.totalBarrelsCreated - totalBarrelInDb + 1 ).ToString();
            ModelBarrel modelLastBarrel = DbBarrel.getInstance().last();
            ModelBarrel modelFirstBarrel = DbBarrel.getInstance().first();
            ModelScore modelScore = DbScore.getInstance().first();
            

            if ( modelLastBarrel.GapCenterX + ( GlobalConstant.BARREL_WIDTH / 2 ) + GlobalConstant.BARREL_SEPARATION < GlobalConstant.SCREEN_WIDTH )
            {
                this.createBarrel();
            }

            if ( modelFirstBarrel.GapCenterX + ( GlobalConstant.BARREL_WIDTH / 2 ) < 0 )
            {
                if ( modelFirstBarrel.Collided )
                {
                    ++modelScore.Collided;
                    modelScore.add( "Collided" );
                }
                else
                {
                    ++modelScore.NotCollided;
                    modelScore.add( "NotCollided" );                    
                }

                DbBarrel.getInstance().delete( 0, out modelFirstBarrel );
            }
            // remove barrel that has move out of screen to conserve memory
             
            // update all barrels
            // s = v * t
            //model.GapCenterX += barrelSpeed;
            List<ModelBarrel> barrelList = DbBarrel.getInstance().getRefList();
            foreach ( ModelBarrel model in barrelList )
            {
                model.GapCenterX += GlobalConstant.BARREL_SPEED;
            }
        }

        public bool checkCollision( int birdCenterX, int birdCenterY, int birdRadius )
        {
            List<ModelBarrel> barrelList = DbBarrel.getInstance().getRefList();            
            foreach ( ModelBarrel model in barrelList )
            {
                // check bird is within the width of the barrel
                bool isBirdWithinBarrelWidth = ( ( model.GapCenterX - ( model.BarrelWidth / 2 ) < ( birdCenterX + birdRadius ) ) && ( ( birdCenterX - birdRadius ) < ( model.GapCenterX + ( model.BarrelWidth / 2 ) ) ) );
                if ( isBirdWithinBarrelWidth )
                {
                    bool isBirdWithinBarrelOpening = ( ( model.GapCenterY - ( model.GapOpeningHeight / 2 ) < ( birdCenterY - birdRadius ) ) && ( ( birdCenterY + birdRadius ) < ( model.GapCenterY + ( model.GapOpeningHeight / 2 ) ) ) );
                    if ( !isBirdWithinBarrelOpening )
                    {
                        model.Collided = true;                        
                        return true;
                    }                    
                }                
            }            
            return false;
        }

        public ModelBarrel getNearestBarrelInFrontOfBird( int birdCenterX, int birdRadius )
        {
            ModelBarrel firstBarrel = DbBarrel.getInstance().first();

            // check if bird has went past the barrel
            // if it went past..the 2nd barrel must be the nearest
            bool isBirdPastBarrel = ( birdCenterX - birdRadius ) > ( firstBarrel.GapCenterX + ( firstBarrel.BarrelWidth / 2 ) );
            if ( isBirdPastBarrel )
            {
                firstBarrel.Nearest = false;
                // there must be a 2nd barrel since it has went past the first
                ModelBarrel secondBarrel = null;
                DbBarrel.getInstance().get( 1, out secondBarrel );
                secondBarrel.Nearest = true;
                return secondBarrel;
            }
            else
            {
                firstBarrel.Nearest = true;
                return firstBarrel;
            }            
        }

        private ModelBarrel createBarrel()
        {
            // draw first barrel to kick start
            ++totalBarrelsCreated;
            ModelBarrel model = new ModelBarrel();
            model.ID = this.totalBarrelsCreated.ToString();
            model.GapOpeningHeight = r.Next( GlobalConstant.BARREL_OPENING_HEIGHT_MIN, GlobalConstant.BARREL_OPENING_HEIGHT_MAX );
            model.GapCenterX = GlobalConstant.SCREEN_WIDTH;
            model.GapCenterY = r.Next( GlobalConstant.BARREL_OPENING_CENTER_MIN, GlobalConstant.BARREL_OPENING_CENTER_MAX );
            model.BarrelWidth = GlobalConstant.BARREL_WIDTH;
            DbBarrel.getInstance().add( model );

            return model;
        }
    }
}
