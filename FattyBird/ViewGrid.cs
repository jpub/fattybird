﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FattyBird
{
    class ViewGrid : BaseView
    {
        private List<Point> listStartY = new List<Point>();
        private List<Point> listEndY = new List<Point>();
        private List<Point> listStartX = new List<Point>();
        private List<Point> listEndX = new List<Point>();
        private List<string> listLabelX = new List<string>();
        private List<string> listLabelY = new List<string>();

        public ViewGrid()
        {                        
        }

        public void init()
        {
            for ( int x = 0; x < GlobalConstant.SCREEN_WIDTH; x += GlobalConstant.GRID_SEPARATION )
            {
                Point startPointX = new Point( x, 0 );
                Point endPointX = new Point( x, GlobalConstant.SCREEN_HEIGHT );
                listLabelX.Add( x.ToString() );
                listStartX.Add( Utility.logicalToPixel( startPointX ) );
                listEndX.Add( Utility.logicalToPixel( endPointX ) );
            }

            for ( int y = 0; y < GlobalConstant.SCREEN_HEIGHT; y += GlobalConstant.GRID_SEPARATION )
            {
                Point startPointY = new Point( 0, y );
                Point endPointY = new Point( GlobalConstant.SCREEN_WIDTH, y );
                listLabelY.Add( y.ToString() );
                listStartY.Add( Utility.logicalToPixel( startPointY ) );
                listEndY.Add( Utility.logicalToPixel( endPointY ) );
            }
        }

        public override void draw( Graphics g )
        {
            if ( !this.isShownOnAsp )
                return;

            int totalCountX = listStartX.Count;
            for ( int i = 0; i < totalCountX; ++i )
            {
                g.DrawLine( GlobalConstant.PEN_GRID, listStartX[i], listEndX[i] );
                g.DrawString( listLabelX[i], GlobalConstant.COEF_FONT_X_SMALL, GlobalConstant.BRUSH_GRID, listStartX[i].X, listStartX[i].Y - 15 );
            }

            int totalCountY = listStartY.Count;
            for ( int i = 0; i < totalCountY; ++i )
            {
                g.DrawLine( GlobalConstant.PEN_GRID, listStartY[i], listEndY[i] );
                g.DrawString( listLabelY[i], GlobalConstant.COEF_FONT_X_SMALL, GlobalConstant.BRUSH_GRID, listStartY[i].X, listStartY[i].Y );
            }       
        }
    }
}
