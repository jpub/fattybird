﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace FattyBird
{
    class ManagerBird
    {
        private static ManagerBird msInstance = null;
        private BirdNet birdNet = null;

        private ManagerBird()
        {            
        }

        static public ManagerBird getInstance()
        {
            if ( msInstance == null )
            {
                msInstance = new ManagerBird();
                return msInstance;
            }
            return msInstance;
        }

        public void initBird()
        {
            ModelBird model = new ModelBird();
            model.Radius = 20;
            model.CenterX = 100;
            model.CenterY = 200;
            DbBird.getInstance().add( model );

            this.birdNet = new BirdNet();
            this.birdNet.init();
        }

        public void updateBird( bool flyUp )
        {
            ModelBird model = DbBird.getInstance().first();

            // v = u + at
            if ( flyUp )
            {
                model.VelY = GlobalConstant.BIRD_THRUST;                
                model.CenterY += model.VelY;

                if ( model.CenterY + model.Radius > GlobalConstant.SCREEN_HEIGHT )
                {
                    model.CenterY = GlobalConstant.SCREEN_HEIGHT - model.Radius;
                    model.VelY = 0;
                }
            }
            else
            {
                int dropDownVel = GlobalConstant.BIRD_GRAVITY;
                model.VelY += dropDownVel;
                // s = v * t
                model.CenterY += model.VelY;

                if ( model.CenterY - model.Radius < 0 )
                    model.CenterY = model.Radius;
            }
            ManagerBarrel.getInstance().checkCollision( model.CenterX, model.CenterY, model.Radius );
        }

        public void selfFlyTheory()
        {
            double action = this.theoreticalAction();
            if ( action > 0.5 )
                this.updateBird( true );
            else
                this.updateBird( false );
        }

        // 1.0 means click
        // 0.0 mean no-click
        private double theoreticalAction()
        {
            ModelBird modelBird = DbBird.getInstance().first();

            ModelBarrel nearestBarrel = ManagerBarrel.getInstance().getNearestBarrelInFrontOfBird( modelBird.CenterX, modelBird.Radius );
            if ( nearestBarrel == null )
                return 0.0;

            // the allowance is for odd number division
            int barrelBottomLine = nearestBarrel.GapCenterY - nearestBarrel.GapOpeningHeight / 2;
            barrelBottomLine += GlobalConstant.BARREL_JUMP_ALLOWANCE;

            // check bird next frame bottom position if it does not fly up
            int birdNextFrameBtmPosition = ( modelBird.CenterY - modelBird.Radius ) + ( modelBird.VelY + GlobalConstant.BIRD_GRAVITY );

            if ( birdNextFrameBtmPosition < barrelBottomLine )
                return 1.0;
            else
                return 0.0;
        }

        public void saveNeuralNetwork()
        {
            this.birdNet.saveWeights();
        }

        public void loadNeuralNetwork()
        {
            this.birdNet.loadWeights();
        }

        public void selfFlyNeuralNetwork()
        {
            double expectedAction = this.theoreticalAction();

            ModelBird modelBird = DbBird.getInstance().first();
            ModelBarrel nearestBarrel = ManagerBarrel.getInstance().getNearestBarrelInFrontOfBird( modelBird.CenterX, modelBird.Radius );

            if ( nearestBarrel == null )
                return;

            bool toClick = this.birdNet.train(
                modelBird.CenterY,
                modelBird.VelY,
                nearestBarrel.GapCenterX,
                nearestBarrel.GapCenterY,
                nearestBarrel.BarrelWidth,
                nearestBarrel.GapOpeningHeight,
                GlobalConstant.BARREL_SPEED,
                expectedAction );

            this.updateBird( toClick );
        }
    }
}
