﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FattyBird
{
    class GlobalConstant
    {
        private static Configuration config = null;

        public static readonly Font COEF_FONT_X_SMALL = new Font( FontFamily.GenericSansSerif, 7.0f );
        public static readonly Font COEF_FONT_SMALL = new Font( FontFamily.GenericSansSerif, 8.0f );
        public static readonly Font COEF_FONT_LARGE = new Font( FontFamily.GenericSansSerif, 16.0f );
        public static readonly Font COEF_FONT_X_LARGE = new Font( FontFamily.GenericSansSerif, 24.0f );

        public static int SCREEN_WIDTH;
        public static int SCREEN_HEIGHT;

        public static int BARREL_SPEED;
        public static int BARREL_SEPARATION;
        public static int BARREL_WIDTH;
        public static int BARREL_OPENING_HEIGHT_MIN;
        public static int BARREL_OPENING_HEIGHT_MAX;
        public static int BARREL_OPENING_CENTER_MIN;
        public static int BARREL_OPENING_CENTER_MAX;
        public static int BARREL_JUMP_ALLOWANCE;
        public static int BIRD_THRUST;
        public static int BIRD_GRAVITY;
        public static int GRID_SEPARATION;

        public static Pen PEN_GENERAL;
        public static Pen PEN_GRID;
        public static Pen PEN_BIRD;
        public static Brush BRUSH_GENERAL;
        public static Brush BRUSH_SCORE_PASS;
        public static Brush BRUSH_SCORE_FAIL;
        public static Brush BRUSH_GRID;
        public static Brush BRUSH_BIRD;
        public static Brush BRUSH_BARREL;
        public static Brush BRUSH_BARREL_COLLIDED;

        public static void init()
        {
            config = ConfigurationManager.OpenExeConfiguration( ConfigurationUserLevel.None );
            BARREL_SPEED = int.Parse( config.AppSettings.Settings["BARREL_SPEED"].Value );
            BARREL_SEPARATION = int.Parse( config.AppSettings.Settings["BARREL_SEPARATION"].Value );
            BARREL_WIDTH = int.Parse( config.AppSettings.Settings["BARREL_WIDTH"].Value );
            BARREL_OPENING_HEIGHT_MIN = int.Parse( config.AppSettings.Settings["BARREL_OPENING_HEIGHT_MIN"].Value );
            BARREL_OPENING_HEIGHT_MAX = int.Parse( config.AppSettings.Settings["BARREL_OPENING_HEIGHT_MAX"].Value );
            BARREL_OPENING_CENTER_MIN = int.Parse( config.AppSettings.Settings["BARREL_OPENING_CENTER_MIN"].Value );
            BARREL_OPENING_CENTER_MAX = int.Parse( config.AppSettings.Settings["BARREL_OPENING_CENTER_MAX"].Value );
            BARREL_JUMP_ALLOWANCE = int.Parse( config.AppSettings.Settings["BARREL_JUMP_ALLOWANCE"].Value );
            BIRD_THRUST = int.Parse( config.AppSettings.Settings["BIRD_THRUST"].Value );
            BIRD_GRAVITY = int.Parse( config.AppSettings.Settings["BIRD_GRAVITY"].Value );
            GRID_SEPARATION = int.Parse( config.AppSettings.Settings["GRID_SEPARATION"].Value );

            PEN_GENERAL = new Pen( Color.Red, 1 );
            PEN_GRID = new Pen( Color.DimGray, 1 );
            PEN_BIRD = new Pen( Color.Red, 1 );

            BRUSH_GENERAL = new SolidBrush( Color.Red );
            BRUSH_SCORE_PASS = new SolidBrush( Color.Yellow );
            BRUSH_SCORE_FAIL = new SolidBrush( Color.Red );
            BRUSH_GRID = new SolidBrush( Color.DimGray );
            BRUSH_BIRD = new SolidBrush( Color.LightBlue );
            BRUSH_BARREL = new SolidBrush( Color.Green );
            BRUSH_BARREL_COLLIDED = new SolidBrush( Color.Pink );
        }
    }
}
