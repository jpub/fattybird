﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FattyBird
{
    class Utility
    {
        static public Point logicalToPixel( Point logicalPoint )
        {
            int newX = ( logicalPoint.X - 0 ) * GlobalConstant.SCREEN_WIDTH / ( GlobalConstant.SCREEN_WIDTH - 0 );
            int newY = GlobalConstant.SCREEN_HEIGHT - ( ( logicalPoint.Y - 0 ) * GlobalConstant.SCREEN_HEIGHT / ( GlobalConstant.SCREEN_HEIGHT - 0 ) );

            return new Point( newX, newY );
        }

        static public Point pixelToLogical( Point pixelPoint )
        {
            int newX = ( pixelPoint.X / GlobalConstant.SCREEN_WIDTH * ( GlobalConstant.SCREEN_WIDTH - 0 ) ) + GlobalConstant.SCREEN_WIDTH;
            int newY = GlobalConstant.SCREEN_HEIGHT - ( ( pixelPoint.Y ) / GlobalConstant.SCREEN_HEIGHT * ( GlobalConstant.SCREEN_HEIGHT - 0 ) );

            return new Point( newX, newY );
        }
    }
}
