﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FattyBird
{
    class ViewBarrel : BaseView
    {
        public override void draw( Graphics g )
        {
            if ( !this.isShownOnAsp )
                return;

            List<ModelBarrel> barrelList = DbBarrel.getInstance().getRefList();

            foreach ( ModelBarrel model in barrelList )
            {
                model.draw( g );
            }
        }
    }
}