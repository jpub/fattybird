﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.Data.Text;


namespace FattyBird
{
    class BirdNet
    {        
        private double learning_rate = 1;
        private int batch_size = 100;
        private int input_total = 7;
        private int hidden1_total = 30;
        private int hidden2_total = 30;
        private int output_total = 1;
        private int train_counter = 0;

        private MatrixBuilder<double> M = null;
        private Matrix<double> W_1 = null;
        private Matrix<double> B_1 = null;

        private Matrix<double> W_2 = null;
        private Matrix<double> B_2 = null;

        private Matrix<double> W_3 = null;
        private Matrix<double> B_3 = null;

        private Matrix<double> batch_DW_1 = null;
        private Matrix<double> batch_DB_1 = null;

        private Matrix<double> batch_DW_2 = null;
        private Matrix<double> batch_DB_2 = null;

        private Matrix<double> batch_DW_3 = null;
        private Matrix<double> batch_DB_3 = null;

        public void init()
        {
            M = Matrix<double>.Build;

            W_1 = M.Random( hidden1_total, input_total );
            B_1 = M.Random( hidden1_total, 1 );

            W_2 = M.Random( hidden2_total, hidden1_total );
            B_2 = M.Random( hidden2_total, 1 );

            W_3 = M.Random( output_total, hidden2_total );
            B_3 = M.Random( output_total, 1 );

            batch_DW_1 = M.Dense( hidden1_total, input_total, 0 );
            batch_DB_1 = M.Dense( hidden1_total, 1, 0 );

            batch_DW_2 = M.Dense( hidden2_total, hidden1_total, 0 );
            batch_DB_2 = M.Dense( hidden2_total, 1, 0 );

            batch_DW_3 = M.Dense( output_total, hidden2_total, 0 );
            batch_DB_3 = M.Dense( output_total, 1, 0 );
        }

        public void saveWeights()
        {
            DelimitedWriter.Write( "W_1.csv", W_1, "," );
            DelimitedWriter.Write( "W_2.csv", W_2, "," );
            DelimitedWriter.Write( "W_3.csv", W_3, "," );
            DelimitedWriter.Write( "B_1.csv", B_1, "," );
            DelimitedWriter.Write( "B_2.csv", B_2, "," );
            DelimitedWriter.Write( "B_3.csv", B_3, "," );
        }

        public void loadWeights()
        {
            W_1 = DelimitedReader.Read<double>( "W_1.csv", false, "," );
            W_2 = DelimitedReader.Read<double>( "W_2.csv", false, "," );
            W_3 = DelimitedReader.Read<double>( "W_3.csv", false, "," );
            B_1 = DelimitedReader.Read<double>( "B_1.csv", false, "," );
            B_2 = DelimitedReader.Read<double>( "B_2.csv", false, "," );
            B_3 = DelimitedReader.Read<double>( "B_3.csv", false, "," );
        }
        // we are feeding in the expected action from the theory to train it..so that I can train it continuosly
        // true = click
        // false = no-click
        public bool train(
            double birdCenterY,
            double birdVelY,
            double barrelCenterX,
            double barrelCenterY,
            double barrelWidth,
            double barrelGapOpeningHeight,
            double barrelSpeed,
            double expectedAction )
        {
            ++train_counter;

            // first forward feed to hidden layer
            Matrix<double> A_0 = M.DenseOfColumnArrays( new double[] {
                birdCenterY,
                birdVelY,
                barrelCenterX,
                barrelCenterY,
                barrelWidth,
                barrelGapOpeningHeight,
                barrelSpeed} );

            Matrix<double> Z_1 = W_1 * A_0 + B_1;
            Matrix<double> A_1 = Z_1.Map( activation_sigmoid );

            // second forward feed to next hidden layer            
            Matrix<double> Z_2 = W_2 * A_1 + B_2;
            Matrix<double> A_2 = Z_2.Map( activation_sigmoid );

            // third forward feed to output layer   
            // we use sigmoid at the last output layer since this is just a logistic regression..just nice 
            Matrix<double> Z_3 = W_3 * A_2 + B_3;
            Matrix<double> A_3 = Z_3.Map( activation_sigmoid );

            // check the cost..the difference between the NN output and the expected output
            // for simplicity, click = value 1, no-click = value 0
            double cost = 0.0;
            Matrix<double> Y = M.Dense( output_total, 1 );
            Y[0, 0] = expectedAction;
            //cost = cost_square_error( Y[0, 0], A_3[0, 0] );
            //cost = cost_logLikelihood( Y[0, 0], A_3[0, 0] );            
            cost = cost_logLogisticRegression( Y[0, 0], A_3[0, 0] );

            // backpropagation
            Matrix<double> DELTA_3 = M.Dense( output_total, 1 );
            for ( int i = 0; i < output_total; ++i )
            {
                DELTA_3[i, 0] = lastLayerSigmoid_partial_c_z( Y[i, 0], A_3[i, 0] );
            }

            Matrix<double> DW_3 = DELTA_3 * ( A_2.Transpose() );
            Matrix<double> DB_3 = DELTA_3;

            Matrix<double> ACTIVATION_DERIVATIVE_2 = A_2.Map( partial_activation_sigmoid );
            Matrix<double> DELTA_2 = ( ( W_3.Transpose() ) * DELTA_3 ).PointwiseMultiply( ACTIVATION_DERIVATIVE_2 );

            //////////////////////////////////////////

            Matrix<double> DW_2 = DELTA_2 * ( A_1.Transpose() );
            Matrix<double> DB_2 = DELTA_2;

            Matrix<double> ACTIVATION_DERIVATIVE_1 = A_1.Map( partial_activation_sigmoid );
            Matrix<double> DELTA_1 = ( ( W_2.Transpose() ) * DELTA_2 ).PointwiseMultiply( ACTIVATION_DERIVATIVE_1 );

            Matrix<double> DW_1 = DELTA_1 * ( A_0.Transpose() );
            Matrix<double> DB_1 = DELTA_1;

            // batch weights update         
            batch_DW_1 = batch_DW_1 + DW_1;
            batch_DB_1 = batch_DB_1 + DB_1;
            batch_DW_2 = batch_DW_2 + DW_2;
            batch_DB_2 = batch_DB_2 + DB_2;
            batch_DW_3 = batch_DW_3 + DW_3;
            batch_DB_3 = batch_DB_3 + DB_3;

            if ( train_counter % batch_size == 0 )
            {
                batch_DW_1 = batch_DW_1 / batch_size;
                batch_DB_1 = batch_DB_1 / batch_size;
                batch_DW_2 = batch_DW_2 / batch_size;
                batch_DB_2 = batch_DB_2 / batch_size;
                batch_DW_3 = batch_DW_3 / batch_size;
                batch_DB_3 = batch_DB_3 / batch_size;

                W_1 = W_1 - learning_rate * batch_DW_1;
                B_1 = B_1 - learning_rate * batch_DB_1;

                W_2 = W_2 - learning_rate * batch_DW_2;
                B_2 = B_2 - learning_rate * batch_DB_2;

                W_3 = W_3 - learning_rate * batch_DW_3;
                B_3 = B_3 - learning_rate * batch_DB_3;

                batch_DW_1.Clear();
                batch_DB_1.Clear();
                batch_DW_2.Clear();
                batch_DB_2.Clear();
                batch_DW_3.Clear();
                batch_DB_3.Clear();
            }

            if ( A_3[0, 0] > 0.5 )
                return true;
            else
                return false;
        }

        #region Helper function
        private double activation_sigmoid( double z )
        {
            return ( 1.0 / ( 1.0 + Math.Exp( -z ) ) );
        }

        private double cost_square_error( double y, double a )
        {
            return ( 0.5 * ( y - a ) * ( y - a ) );
        }

        private double cost_logLikelihood( double y, double a )
        {
            return y == 0 ? 0 : -( y * Math.Log( a ) );
        }

        private double cost_logLogisticRegression( double y, double a )
        {
            // Loss = -(y*log_a + (1-y)*log_(1-a))
            // if y == 1: Loss = -log_a
            // if y == 0: Loss = -log_(1-a)
            return y == 0 ? -( Math.Log( 1 - a ) ) : -( Math.Log( a ) );
        }

        private double lastLayerSigmoid_partial_c_z( double y, double a )
        {
            // dc /dz = dc/da * da/dz
            // dc /da = -(y-a) = (a-y)
            // da /dz = a(1-a) # for sigmoid
            return ( a - y ) * a * ( 1 - a );
        }

        private double lastLayerSoftmax_partial_c_z( double y, double a )
        {
            return ( a - y );
        }

        private double partial_activation_sigmoid( double a )
        {
            return a * ( 1 - a );
        }

        private double partial_c_w( double input, double delta_cz )
        {
            // dc /dw = (dc/da * da/dz) * dz/dw
            // dz /dw = a
            // input = the input (previous a) feeding to the neuron corresponding weight w
            // delta_cz = partial_c_z (for output layer)
            return delta_cz * input;
        }
        #endregion
    }
}
