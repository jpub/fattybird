﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

namespace FattyBird
{
    public partial class FormMain : Form
    {
        private List<BaseView> fattyListView = new List<BaseView>();        
        private ViewBarrel viewBarrel = new ViewBarrel();
        private ViewBird viewBird = new ViewBird();
        private ViewGrid viewGrid = new ViewGrid();
        private ViewScore viewScore = new ViewScore();

        public FormMain()
        {
            InitializeComponent();
            GlobalConstant.init();            
            GlobalConstant.SCREEN_WIDTH = this.panelFatty.Width;
            GlobalConstant.SCREEN_HEIGHT = this.panelFatty.Height;
            this.viewGrid.init();

            this.fattyListView.Add( viewBarrel );
            this.fattyListView.Add( viewBird );            
            this.fattyListView.Add( viewGrid );
            this.fattyListView.Add( viewScore );

            ManagerBird.getInstance().initBird();
            //ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.DoubleBuffer
            // use reflection to set the double buffer property instead of creating a subclass because this property is protected
            typeof( Control ).InvokeMember( "DoubleBuffered",
                BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                null, this.panelFatty, new object[] { true } );

            this.initChart( chart1 );
        }

        private void timerAnimation_Tick( object sender, EventArgs e )
        {
            ManagerBarrel.getInstance().updateBarrels();

            if ( this.toolStripButtonToggleTheory.Checked )
                ManagerBird.getInstance().selfFlyTheory();
            else if ( this.toolStripButtonToggleNeuralNetwork.Checked )
                ManagerBird.getInstance().selfFlyNeuralNetwork();
            else
                ManagerBird.getInstance().updateBird( false );

            this.panelFatty.Refresh();
        }

        private void panelFatty_Paint( object sender, PaintEventArgs e )
        {
            foreach ( BaseView view in this.fattyListView )
                view.draw( e.Graphics );
        }

        private void panelFatty_MouseDown( object sender, MouseEventArgs e )
        {
            ManagerBird.getInstance().updateBird( true );
        }

        private void toolStripButtonSaveWeights_Click( object sender, EventArgs e )
        {
            ManagerBird.getInstance().saveNeuralNetwork();
        }

        private void toolStripButtonLoadWeights_Click( object sender, EventArgs e )
        {
            ManagerBird.getInstance().loadNeuralNetwork();
        }

        private void initChart( System.Windows.Forms.DataVisualization.Charting.Chart inChart )
        {
            inChart.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            inChart.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
            inChart.ChartAreas[0].AxisX.MinorGrid.Enabled = false;
            inChart.ChartAreas[0].AxisY.MinorGrid.Enabled = false;
            inChart.ChartAreas[0].AxisX.MajorTickMark.Enabled = false;
            inChart.ChartAreas[0].AxisY.MajorTickMark.Enabled = false;
            inChart.ChartAreas[0].AxisX.MinorTickMark.Enabled = false;
            inChart.ChartAreas[0].AxisY.MinorTickMark.Enabled = false;

            inChart.ChartAreas[0].AxisX.IsStartedFromZero = false;
            inChart.ChartAreas[0].AxisY.IsStartedFromZero = false;

            inChart.BackColor = Color.Black;
            inChart.ChartAreas[0].BackColor = Color.Black;
            inChart.ChartAreas[0].AxisX.LineColor = Color.Yellow;
            inChart.ChartAreas[0].AxisY.LineColor = Color.Yellow;
            inChart.ChartAreas[0].AxisX.LabelStyle.ForeColor = Color.Aqua;
            inChart.ChartAreas[0].AxisY.LabelStyle.ForeColor = Color.Aqua;
            inChart.Titles[0].ForeColor = Color.Aqua;
            inChart.ChartAreas[0].AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            inChart.ChartAreas[0].AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            inChart.BorderlineWidth = 1;
            inChart.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            inChart.BorderlineColor = Color.Yellow;
        }

        private void timerChart_Tick( object sender, EventArgs e )
        {
            ModelScore modelScore = DbScore.getInstance().first();
            List<double> accuracyChartValueList = modelScore.getAccuracyChartValueList();
            int totalRecordedCount = accuracyChartValueList.Count;
            if ( totalRecordedCount == 0 )
                return;

            if ( this.chart1.Series[0].Points.Count != totalRecordedCount )
            {
                this.chart1.Series[0].Points.AddXY( totalRecordedCount, accuracyChartValueList[totalRecordedCount - 1] );
            }
        }
    }
}
