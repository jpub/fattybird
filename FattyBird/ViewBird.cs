﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FattyBird
{
    class ViewBird : BaseView
    {
        public override void draw( Graphics g )
        {
            if ( !this.isShownOnAsp )
                return;

            List<ModelBird> birdList = DbBird.getInstance().getRefList();

            foreach ( ModelBird model in birdList )
            {
                model.draw( g );
            }
        }
    }
}