﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FattyBird
{
    class ModelBarrel : BaseModel
    {
        #region Properties        
        public bool Collided { get; set; }
        public bool Nearest { get; set; }
        public int BarrelWidth { get; set; }
        public int GapOpeningHeight { get; set; }
        public int GapCenterX { get; set; }
        public int GapCenterY { get; set; }        
        #endregion

        public ModelBarrel()
        {
            this.Collided = false;
            this.Nearest = false;
        }

        public override bool draw( Graphics g )
        {
            /*
            Point logicalCenterPoint = new Point( 100, 300 );
            Point pixelCenterPoint = this.logicalToPixel( logicalCenterPoint );
            int radius = 50;
            Point pixelStartPoint = new Point( pixelCenterPoint.X - ( radius / 2 ), pixelCenterPoint.Y - ( radius / 2 ) );
            g.DrawEllipse( GlobalConstant.PEN_GENERAL, pixelStartPoint.X, pixelStartPoint.Y, radius, radius );
            */

            Point logicalCenterPoint = new Point( GapCenterX, GapCenterY );
            Point pixelCenterPoint = Utility.logicalToPixel( logicalCenterPoint );

            int topBarrelLeftStartPoint = pixelCenterPoint.X - BarrelWidth / 2;
            int topBarrelTopStartPoint = 0;
            int topBarrelLength = pixelCenterPoint.Y - this.GapOpeningHeight / 2;

            int btmBarrelLeftStartPoint = topBarrelLeftStartPoint;
            int btmBarrelTopStartPoint = pixelCenterPoint.Y + this.GapOpeningHeight / 2;
            int btmBarrelLength = GlobalConstant.SCREEN_HEIGHT - btmBarrelTopStartPoint;

            Brush barrelBrush = this.Collided ? GlobalConstant.BRUSH_BARREL_COLLIDED : GlobalConstant.BRUSH_BARREL;
            // top barrel
            g.FillRectangle( barrelBrush, topBarrelLeftStartPoint, topBarrelTopStartPoint, this.BarrelWidth, topBarrelLength );

            // bottom barrel
            g.FillRectangle( barrelBrush, btmBarrelLeftStartPoint, btmBarrelTopStartPoint, this.BarrelWidth, btmBarrelLength );

            if ( this.Nearest )
            {
                //g.DrawLine( GlobalConstant.PEN_GENERAL, btmBarrelLeftStartPoint, btmBarrelTopStartPoint - GlobalConstant.BARREL_JUMP_ALLOWANCE, 0, btmBarrelTopStartPoint - GlobalConstant.BARREL_JUMP_ALLOWANCE );
            }
            return true;
        }
    }
}

