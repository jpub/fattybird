﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FattyBird
{
    class DbScore : BaseListDb<ModelScore>
    {
        private static DbScore msInstance = null;

        private DbScore()
        {
        }

        static public DbScore getInstance()
        {
            if ( msInstance == null )
            {
                msInstance = new DbScore();
                return msInstance;
            }
            return msInstance;
        }
    }
}
