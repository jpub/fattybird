﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FattyBird
{
    abstract public class BaseListDb<T>
    {
        protected object lockMe = new object();
        protected List<T> dbList = new List<T>();

        protected BaseListDb()
        {
        }

        virtual public void clear()
        {
            lock ( lockMe )
            {
                this.dbList.Clear();
            }
        }

        virtual public int count()
        {
            lock ( lockMe )
            {
                return this.dbList.Count;
            }
        }

        virtual public void update( int index, T item )
        {
            lock ( lockMe )
            {
                this.dbList[index] = item;
            }
        }

        virtual public void add( T item )
        {
            lock ( lockMe )
            {
                this.dbList.Add( item );
            }
        }

        virtual public List<T> getRefList()
        {
            lock ( lockMe )
            {
                List<T> valueList = new List<T>( this.dbList );
                return valueList;
            }
        }

        virtual public bool get( int index, out T item )
        {
            lock ( lockMe )
            {
                if ( index >= this.dbList.Count )
                {
                    item = default( T );
                    return false;
                }

                item = this.dbList[index];
                return true;
            }
        }

        virtual public T first()
        {
            lock ( lockMe )
            {
                return this.dbList.First();
            }
        }

        virtual public T last()
        {
            lock ( lockMe )
            {
                return this.dbList.Last();
            }
        }

        virtual public bool delete( int index, out T deletedItem )
        {            
            lock ( lockMe )
            {
                if ( index >= this.dbList.Count )
                {
                    deletedItem = default( T );
                    return false;
                }
                else
                {
                    deletedItem = this.dbList[index];
                    this.dbList.RemoveAt( index );
                    return true;
                }
            }
        }
    }
}
