﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FattyBird
{
    class ModelScore : BaseModel
    {
        private List<string> accuracySegmentList = null;
        private List<double> accuracyChartValueList = null;        

        #region Properties
        public int Collided { get; set; }
        public int NotCollided { get; set; }        
        #endregion

        public ModelScore()
        {
            this.accuracySegmentList = new List<string>();
            this.accuracyChartValueList = new List<double>();
            this.accuracyChartValueList.Add( 0.0 );
        }

        public void add( string collisionString )
        {
            lock(this)
            {
                this.accuracySegmentList.Add( collisionString );
                if ( this.accuracySegmentList.Count == 100 )
                {
                    int notCollisionCount = this.accuracySegmentList
                        .Where( temp => temp.Equals( "NotCollided" ) )
                        .Select( temp => temp )
                        .Count();

                    this.accuracyChartValueList.Add( notCollisionCount / 100.0 );
                    this.accuracySegmentList.Clear();
                }
            }
        }

        public List<double> getAccuracyChartValueList()
        {
            List<double> valueList = new List<double>( this.accuracyChartValueList );
            return valueList;
        }

        public override bool draw( Graphics g )
        {
            g.DrawString( "PASS:", GlobalConstant.COEF_FONT_LARGE, GlobalConstant.BRUSH_SCORE_PASS, 10, 10 );
            g.DrawString( this.NotCollided.ToString(), GlobalConstant.COEF_FONT_LARGE, GlobalConstant.BRUSH_SCORE_PASS, 80, 10 );

            g.DrawString( "FAIL:", GlobalConstant.COEF_FONT_LARGE, GlobalConstant.BRUSH_SCORE_FAIL, 10, 30 );
            g.DrawString( this.Collided.ToString(), GlobalConstant.COEF_FONT_LARGE, GlobalConstant.BRUSH_SCORE_FAIL, 80, 30 );

            //g.DrawString( "Acc:", GlobalConstant.COEF_FONT_LARGE, GlobalConstant.BRUSH_SCORE_FAIL, 10, 50 );
            //g.DrawString( this.accuracy.ToString(), GlobalConstant.COEF_FONT_LARGE, GlobalConstant.BRUSH_SCORE_FAIL, 80, 50 );

            return true;
        }
    }
}
