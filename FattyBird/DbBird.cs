﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FattyBird
{
    class DbBird : BaseListDb<ModelBird>
    {
        private static DbBird msInstance = null;

        private DbBird()
        {
        }

        static public DbBird getInstance()
        {
            if ( msInstance == null )
            {
                msInstance = new DbBird();
                return msInstance;
            }
            return msInstance;
        }
    }
}
