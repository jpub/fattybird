﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FattyBird
{
    abstract public class BaseModel
    {
        abstract public bool draw( Graphics g );

        #region Properties
        public BaseMessage MessageContent { get; set; }
        public string ID { get; set; }
        public Image Symbol { get; set; }
        public bool Hidden { get; set; }
        #endregion

        protected bool isVisibleOnScreen( PointF symbolCenterPoint )
        {
            return ( symbolCenterPoint.X > 0 && symbolCenterPoint.Y > 0 );
        }

        protected bool isVisibleOnScreen( Point symbolCenterPoint )
        {
            return ( symbolCenterPoint.X > 0 && symbolCenterPoint.Y > 0 );
        }        
    }
}
