﻿namespace FattyBird
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint1 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonTogglePause = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonToggleGrid = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonUdpSniffer = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonToggleTheory = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonToggleNeuralNetwork = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSaveWeights = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonLoadWeights = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.panelFatty = new System.Windows.Forms.Panel();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.timerAnimation = new System.Windows.Forms.Timer(this.components);
            this.timerChart = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panelFatty.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonTogglePause,
            this.toolStripButtonToggleGrid,
            this.toolStripButtonUdpSniffer,
            this.toolStripButtonToggleTheory,
            this.toolStripButtonToggleNeuralNetwork,
            this.toolStripButtonSaveWeights,
            this.toolStripButtonLoadWeights});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(700, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonTogglePause
            // 
            this.toolStripButtonTogglePause.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonTogglePause.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonTogglePause.Image")));
            this.toolStripButtonTogglePause.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonTogglePause.Name = "toolStripButtonTogglePause";
            this.toolStripButtonTogglePause.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonTogglePause.Text = "toolStripButton1";
            // 
            // toolStripButtonToggleGrid
            // 
            this.toolStripButtonToggleGrid.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonToggleGrid.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonToggleGrid.Image")));
            this.toolStripButtonToggleGrid.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonToggleGrid.Name = "toolStripButtonToggleGrid";
            this.toolStripButtonToggleGrid.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonToggleGrid.Text = "toolStripButton2";
            // 
            // toolStripButtonUdpSniffer
            // 
            this.toolStripButtonUdpSniffer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonUdpSniffer.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonUdpSniffer.Image")));
            this.toolStripButtonUdpSniffer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonUdpSniffer.Name = "toolStripButtonUdpSniffer";
            this.toolStripButtonUdpSniffer.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonUdpSniffer.Text = "toolStripButton1";
            // 
            // toolStripButtonToggleTheory
            // 
            this.toolStripButtonToggleTheory.CheckOnClick = true;
            this.toolStripButtonToggleTheory.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonToggleTheory.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonToggleTheory.Image")));
            this.toolStripButtonToggleTheory.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonToggleTheory.Name = "toolStripButtonToggleTheory";
            this.toolStripButtonToggleTheory.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonToggleTheory.Text = "Toggle Theory";
            // 
            // toolStripButtonToggleNeuralNetwork
            // 
            this.toolStripButtonToggleNeuralNetwork.Checked = true;
            this.toolStripButtonToggleNeuralNetwork.CheckOnClick = true;
            this.toolStripButtonToggleNeuralNetwork.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripButtonToggleNeuralNetwork.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonToggleNeuralNetwork.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonToggleNeuralNetwork.Image")));
            this.toolStripButtonToggleNeuralNetwork.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonToggleNeuralNetwork.Name = "toolStripButtonToggleNeuralNetwork";
            this.toolStripButtonToggleNeuralNetwork.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonToggleNeuralNetwork.Text = "Toggle Neural Network";
            // 
            // toolStripButtonSaveWeights
            // 
            this.toolStripButtonSaveWeights.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSaveWeights.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSaveWeights.Image")));
            this.toolStripButtonSaveWeights.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSaveWeights.Name = "toolStripButtonSaveWeights";
            this.toolStripButtonSaveWeights.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonSaveWeights.Text = "Save Weights";
            this.toolStripButtonSaveWeights.Click += new System.EventHandler(this.toolStripButtonSaveWeights_Click);
            // 
            // toolStripButtonLoadWeights
            // 
            this.toolStripButtonLoadWeights.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonLoadWeights.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonLoadWeights.Image")));
            this.toolStripButtonLoadWeights.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLoadWeights.Name = "toolStripButtonLoadWeights";
            this.toolStripButtonLoadWeights.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonLoadWeights.Text = "Load Weights";
            this.toolStripButtonLoadWeights.Click += new System.EventHandler(this.toolStripButtonLoadWeights_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 525);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(700, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // panelFatty
            // 
            this.panelFatty.BackColor = System.Drawing.Color.Black;
            this.panelFatty.Controls.Add(this.chart1);
            this.panelFatty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFatty.Location = new System.Drawing.Point(0, 25);
            this.panelFatty.Name = "panelFatty";
            this.panelFatty.Size = new System.Drawing.Size(700, 500);
            this.panelFatty.TabIndex = 2;
            this.panelFatty.Paint += new System.Windows.Forms.PaintEventHandler(this.panelFatty_Paint);
            this.panelFatty.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelFatty_MouseDown);
            // 
            // chart1
            // 
            chartArea1.AxisX.LabelAutoFitMaxFontSize = 6;
            chartArea1.AxisX2.LabelAutoFitMaxFontSize = 6;
            chartArea1.AxisY.LabelAutoFitMaxFontSize = 6;
            chartArea1.AxisY2.LabelAutoFitMaxFontSize = 6;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Location = new System.Drawing.Point(469, 13);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series1.Name = "Series1";
            series1.Points.Add(dataPoint1);
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(219, 147);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            title1.Name = "Title1";
            title1.Text = "Accuracy";
            this.chart1.Titles.Add(title1);
            // 
            // timerAnimation
            // 
            this.timerAnimation.Enabled = true;
            this.timerAnimation.Interval = 1;
            this.timerAnimation.Tick += new System.EventHandler(this.timerAnimation_Tick);
            // 
            // timerChart
            // 
            this.timerChart.Enabled = true;
            this.timerChart.Interval = 1000;
            this.timerChart.Tick += new System.EventHandler(this.timerChart_Tick);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 547);
            this.Controls.Add(this.panelFatty);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "Form1";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panelFatty.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonTogglePause;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Panel panelFatty;
        private System.Windows.Forms.Timer timerAnimation;
        private System.Windows.Forms.ToolStripButton toolStripButtonToggleGrid;
        private System.Windows.Forms.ToolStripButton toolStripButtonUdpSniffer;
        private System.Windows.Forms.ToolStripButton toolStripButtonToggleTheory;
        private System.Windows.Forms.ToolStripButton toolStripButtonToggleNeuralNetwork;
        private System.Windows.Forms.ToolStripButton toolStripButtonSaveWeights;
        private System.Windows.Forms.ToolStripButton toolStripButtonLoadWeights;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Timer timerChart;
    }
}

