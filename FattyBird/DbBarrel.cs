﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FattyBird
{
    class DbBarrel : BaseListDb<ModelBarrel>
    {
        private static DbBarrel msInstance = null;

        private DbBarrel()
        {
        }

        static public DbBarrel getInstance()
        {
            if ( msInstance == null )
            {
                msInstance = new DbBarrel();
                return msInstance;
            }
            return msInstance;
        }
    }
}
