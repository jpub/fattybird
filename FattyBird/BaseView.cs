﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FattyBird
{
    abstract public class BaseView
    {
        protected bool isShownOnAsp = true;
        abstract public void draw( Graphics g );
    }
}
