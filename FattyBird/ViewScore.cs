﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FattyBird
{
    class ViewScore : BaseView
    {
        public override void draw( Graphics g )
        {
            if ( !this.isShownOnAsp )
                return;

            List<ModelScore> scoreList = DbScore.getInstance().getRefList();

            foreach ( ModelScore model in scoreList )
            {
                model.draw( g );
            }
        }
    }
}