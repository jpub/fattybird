﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FattyBird
{
    abstract public class BaseMessage
    {
        static public readonly string ID_MSG_ANY = "62";

        protected BaseMessage()
        {
        }

        virtual public void preprocessing()
        {
        }

        #region Properties
        public string MessageName { get; set; }
        #endregion
    }
}
