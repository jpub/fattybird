﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FattyBird
{
    abstract public class BaseDb<T>
    {
        protected object lockMe = new object();
        protected Dictionary<string, T> dbMap = new Dictionary<string, T>();

        protected BaseDb()
        {
        }

        virtual public void clear()
        {
            lock ( lockMe )
            {
                this.dbMap.Clear();
            }
        }

        virtual public int count()
        {
            lock ( lockMe )
            {
                return this.dbMap.Count;
            }
        }

        virtual public void update( string id, T item )
        {
            lock ( lockMe )
            {
                if ( this.dbMap.ContainsKey( id ) )
                    this.dbMap[id] = item;
                else
                    this.dbMap.Add( id, item );
            }            
        }

        virtual public List<T> getRefList()
        {
            lock ( lockMe )
            {
                List<T> valueList = new List<T>( this.dbMap.Values );
                return valueList;
            }
        }

        virtual public bool get( string id, out T item )
        {
            lock ( lockMe )
            {
                if ( !this.dbMap.ContainsKey( id ) )
                {
                    item = default( T );
                    return false;
                }

                item = this.dbMap[id];
                return true;
            }
        }

        virtual public bool contain( string id )
        {
            lock ( lockMe )
            {
                return this.dbMap.ContainsKey( id );
            }
        }

        virtual public bool delete( string id, out T deletedItem )
        {
            bool status = false;
            lock ( lockMe )
            {
                status = false;
                deletedItem = default( T );

                if ( this.dbMap.ContainsKey( id ) )
                {
                    deletedItem = this.dbMap[id];
                    this.dbMap.Remove( id );
                    status = true;
                }
            }
            
            return status;
        }

        virtual public void deleteList( List<string> idList )
        {
            lock ( lockMe )
            {
                foreach ( string id in idList )
                {
                    if ( this.dbMap.ContainsKey( id ) )
                    {
                        this.dbMap.Remove( id );
                    }
                }
            }
        }
    }
}
