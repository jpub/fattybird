﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FattyBird
{
    class ModelBird : BaseModel
    {
        #region Properties
        public int Radius { get; set; }
        public int CenterX { get; set; }
        public int CenterY { get; set; }
        public int VelX { get; set; }
        public int VelY { get; set; }
        #endregion

        public override bool draw( Graphics g )
        {
            Point logicalCenterPoint = new Point( this.CenterX, this.CenterY );
            Point pixelCenterPoint = Utility.logicalToPixel( logicalCenterPoint );

            Point pixelStartPoint = new Point( pixelCenterPoint.X - this.Radius, pixelCenterPoint.Y - this.Radius );
            g.FillEllipse( GlobalConstant.BRUSH_BIRD, pixelStartPoint.X, pixelStartPoint.Y, this.Radius * 2, this.Radius * 2 );

            return true;
        }
    }
}

